/*
 * demo_bitBanger.cpp
 *
 *  Created on: 6 Nov 2019
 *      Author: Paris Moschovakos
 *
 *  Program to bit bang GPIO to imitate other interfaces
 */

#include <ClientSessionFactory.h>
#include <uaplatformlayer.h>
#include <iomanip>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <IoBatch.h>

using namespace boost::program_options;
using namespace UaoClientForOpcUaSca;

int main( int argc, char* argv[])
{
    UaPlatformLayer::init();

    options_description options;
    std::string opcUaEndpoint;
    std::string bbAddress;
    std::string logLevelStr;
    Log::LOG_LEVEL       logLevel;
    options.add_options()
            ("help,h",          "show help")
            ("endpoint,e",      value<std::string>(&opcUaEndpoint)->default_value("opc.tcp://127.0.0.1:48020"), "OPC UA Endpoint, e.g. opc.tcp://127.0.0.1:48020" )
            ("address,a",       value<std::string>(&bbAddress)->default_value("MMFE8_0007.gpio.bitBanger"),     "Address of the BitBanger")
            ("trace_level,t",   value<std::string>(&logLevelStr)->default_value("INF"),                         "Trace level, one of: ERR,WRN,INF,DBG,TRC")
            ;


    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    if (! Log::logLevelFromString( logLevelStr, logLevel ) )
    {
        std::cout << "Log level not recognized: '" << logLevelStr << "'" << std::endl;
        exit(1);
    }

    Log::initializeLogging(Log::INF);

    UaClientSdk::UaSession* session = ClientSessionFactory::connect(opcUaEndpoint.c_str());
    if (!session)
        return -1;

    try
	{

        IoBatch ioBatch( session, UaNodeId(bbAddress.c_str(), 2) );

        ioBatch.addSetPinsDirections( { { 17, IoBatch::OUTPUT }, {18, IoBatch::OUTPUT } } );
        for ( int i = 0; i < 30; ++i )
        {
        	ioBatch.addSetPins( { { 17, true }, { 18, true } }, 10 );
        	ioBatch.addSetPins( { { 17, false }, { 18, false } }, 3 );
        }

        auto interestingReplies = ioBatch.dispatch();

        std::for_each(
            interestingReplies.begin(), 
            interestingReplies.end(), 
            []( OpcUa_UInt32 reply ){ LOG(Log::INF) << "Reply: 0x" << std::setfill('0') << std::setw(8) << std::hex << reply; }
        );

        std::vector<bool> pinValues = repliesToPinBits( interestingReplies, 17 );

        std::for_each(
            pinValues.begin(), 
            pinValues.end(), 
            []( bool pinValue ){ LOG(Log::INF) << "Pin Value: 0b" << pinValue; }
        );

        ServiceSettings sessset = ServiceSettings();
        session->disconnect(sessset, OpcUa_True);
        delete session;

    }
    catch ( const std::exception &e )
	{

		LOG(Log::ERR) << "exception " << e.what();

	}

}
