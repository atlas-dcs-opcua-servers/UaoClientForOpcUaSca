

#include <iostream>
#include <JtagPort.h>
#include <uaclient/uasession.h>
#include <stdexcept>
#include <UaoClientForOpcUaScaArrayTools.h>
#include <UaoClientForOpcUaScaUaoExceptions.h>

namespace UaoClientForOpcUaSca
{


JtagPort::
JtagPort
(
    UaSession* session,
    UaNodeId objId
) :
    m_session(session),
    m_objId (objId)
{

}



UaString JtagPort::readState (
    UaStatus *out_status,
    UaDateTime *sourceTimeStamp,
    UaDateTime *serverTimeStamp)
{

    ServiceSettings   ss;
    UaReadValueIds    nodesToRead;
    UaDataValues      dataValues;
    UaDiagnosticInfos diagnosticInfos;

    UaNodeId nodeId ( UaString(m_objId.identifierString()) + UaString(".state"), m_objId.namespaceIndex() );

    nodesToRead.create(1);
    nodeId.copyTo( &nodesToRead[0].NodeId );
    nodesToRead[0].AttributeId = OpcUa_Attributes_Value;

    dataValues.create (1);

    UaStatus status = m_session->read(
                          ss,
                          0 /*max age*/,
                          OpcUa_TimestampsToReturn_Both,
                          nodesToRead,
                          dataValues,
                          diagnosticInfos
                      );
    if (status.isBad())
        throw Exceptions::BadStatusCode("OPC-UA read failed", status.statusCode());
    if (out_status)
        *out_status = dataValues[0].StatusCode;
    else
    {
        if (! UaStatus(dataValues[0].StatusCode).isGood())
            throw Exceptions::BadStatusCode("OPC-UA read: variable status is not good", dataValues[0].StatusCode );
    }

    UaString out;


    out = UaVariant(dataValues[0].Value).toString();


    if (sourceTimeStamp)
        *sourceTimeStamp = dataValues[0].SourceTimestamp;
    if (serverTimeStamp)
        *serverTimeStamp = dataValues[0].ServerTimestamp;

    return out;
}


void  JtagPort::writeState (
    UaString& data,
    UaStatus                                 *out_status)
{
    ServiceSettings   ss;
    UaWriteValues    nodesToWrite;
    UaDataValues      dataValues;
    UaDiagnosticInfos diagnosticInfos;
    UaStatusCodeArray results;

    UaNodeId nodeId ( UaString(m_objId.identifierString()) + UaString(".state"), m_objId.namespaceIndex()  );

    nodesToWrite.create(1);
    nodeId.copyTo( &nodesToWrite[0].NodeId );
    nodesToWrite[0].AttributeId = OpcUa_Attributes_Value;

    UaVariant v ( data );

    dataValues.create (1);
    v.copyTo( &nodesToWrite[0].Value.Value );

    UaStatus status = m_session->write(
                          ss,
                          nodesToWrite,
                          results,
                          diagnosticInfos
                      );
    if (out_status)
    {
        *out_status = status;
    }
    else
    {
        if (status.isBad())
            throw Exceptions::BadStatusCode("OPC-UA write failed", status.statusCode() );
        if (results[0] != OpcUa_Good)
            throw Exceptions::BadStatusCode ("OPC-UA write failed", results[0] );
    }

}


void JtagPort::shiftIr (
    OpcUa_UInt32 in_numberOfBits,
    const UaByteString&  in_tdoBits,
    OpcUa_Boolean in_readTdi,
    UaByteString& out_tdiBits
)
{

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = m_objId;
    callRequest.methodId = UaNodeId( UaString(m_objId.identifierString()) + UaString(".shiftIr"), 2 );


    UaVariant v;


    callRequest.inputArguments.create( 3 );

    v.setUInt32( in_numberOfBits );



    v.copyTo( &callRequest.inputArguments[ 0 ] );

    v.setByteString( const_cast<UaByteString&>(in_tdoBits), false );



    v.copyTo( &callRequest.inputArguments[ 1 ] );

    v.setBool( in_readTdi );



    v.copyTo( &callRequest.inputArguments[ 2 ] );



    UaStatus status =
        m_session->call(
            serviceSettings,
            callRequest,
            co
        );
    if (status.isBad())
        throw Exceptions::BadStatusCode("In OPC-UA call", status.statusCode());


    v = co.outputArguments[0];

    v.toByteString (out_tdiBits);



}


void JtagPort::shiftDr (
    OpcUa_UInt32 in_numberOfBits,
    const UaByteString&  in_tdoBits,
    OpcUa_Boolean in_readTdi,
    UaByteString& out_tdiBits
)
{

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = m_objId;
    callRequest.methodId = UaNodeId( UaString(m_objId.identifierString()) + UaString(".shiftDr"), 2 );


    UaVariant v;


    callRequest.inputArguments.create( 3 );

    v.setUInt32( in_numberOfBits );



    v.copyTo( &callRequest.inputArguments[ 0 ] );

    v.setByteString( const_cast<UaByteString&>(in_tdoBits), false );



    v.copyTo( &callRequest.inputArguments[ 1 ] );

    v.setBool( in_readTdi );



    v.copyTo( &callRequest.inputArguments[ 2 ] );



    UaStatus status =
        m_session->call(
            serviceSettings,
            callRequest,
            co
        );
    if (status.isBad())
        throw Exceptions::BadStatusCode("In OPC-UA call", status.statusCode());


    v = co.outputArguments[0];

    v.toByteString (out_tdiBits);



}


void JtagPort::shift (
    OpcUa_UInt32 in_numberOfBits,
    const UaByteString&  in_tmsBits,
    const UaByteString&  in_tdoBits,
    UaByteString& out_tdiBits
)
{

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = m_objId;
    callRequest.methodId = UaNodeId( UaString(m_objId.identifierString()) + UaString(".shift"), 2 );


    UaVariant v;


    callRequest.inputArguments.create( 3 );

    v.setUInt32( in_numberOfBits );



    v.copyTo( &callRequest.inputArguments[ 0 ] );

    v.setByteString( const_cast<UaByteString&>(in_tmsBits), false );



    v.copyTo( &callRequest.inputArguments[ 1 ] );

    v.setByteString( const_cast<UaByteString&>(in_tdoBits), false );



    v.copyTo( &callRequest.inputArguments[ 2 ] );



    UaStatus status =
        m_session->call(
            serviceSettings,
            callRequest,
            co
        );
    if (status.isBad())
        throw Exceptions::BadStatusCode("In OPC-UA call", status.statusCode());


    v = co.outputArguments[0];

    v.toByteString (out_tdiBits);



}



}


